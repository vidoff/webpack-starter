const Path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const fs = require("fs");

function generateHtmlPlugins(templateDir) {
    const templateFiles = fs.readdirSync(Path.resolve(__dirname, templateDir), {
        withFileTypes: true
    });
    return templateFiles
        .filter(templateFiles => !templateFiles.isDirectory())
        .map(item => {
            const parts = item.name.split(".");
            const name = parts[0];
            const extension = parts[1];
            return new HtmlWebpackPlugin({
                filename: `${name}.html`,
                template: Path.resolve(
                    __dirname,
                    `${templateDir}/${name}.${extension}`
                ),
                minify: false
            });
        });
}

const htmlPlugins = generateHtmlPlugins("../src");

module.exports = {
    entry: {
        app: Path.resolve(__dirname, "../src/scripts/main.js")
    },
    output: {
        path: Path.join(__dirname, "../dist"),
        filename: "js/[name].js"
    },
    optimization: {
        splitChunks: {
            chunks: "all",
            name: false
        }
    },
    plugins: [
        // new CleanWebpackPlugin(),
        new CopyWebpackPlugin([
            { from: Path.resolve(__dirname, "../public"), to: "public" }
        ])
    ].concat(htmlPlugins),
    resolve: {
        alias: {
            "~": Path.resolve(__dirname, "../src")
        }
    },
    module: {
        rules: [
            {
                test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
                use: {
                    loader: "url-loader",
                    options: {
                        limit: 100000,
                        name: "[path][hash][name].[ext]"
                    }
                }
            }
        ]
    }
};
