const Path = require("path");
const Webpack = require("webpack");
const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const common = require("./webpack.common.js");

module.exports = merge(common, {
    mode: "development",
    // devtool: "cheap-eval-source-map",
    devtool: "source-map",
    output: {
        chunkFilename: "js/[name].chunk.js",
        filename: "js/bundle_dev.js"
    },
    devServer: {
        inline: true
    },
    plugins: [
        new Webpack.DefinePlugin({
            "process.env.NODE_ENV": JSON.stringify("development")
        }),
        new MiniCssExtractPlugin({
            filename: "css/bundle_dev.css"
        })
    ],
    module: {
        rules: [
            {
                test: /\.(js)$/,
                include: Path.resolve(__dirname, "../src"),
                enforce: "pre",
                loader: "eslint-loader",
                options: {
                    emitWarning: true
                }
            },
            {
                test: /\.(js)$/,
                include: Path.resolve(__dirname, "../src"),
                loader: "babel-loader"
            },
            {
                test: /\.s?css$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: { hmr: process.env.NODE_ENV === "development" }
                    },
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1,
                            camelCase: true
                        }
                    },
                    "postcss-loader"
                ]
            }
        ]
    }
});
