const path = require('path');
module.exports = {
    plugins: [
        require("postcss-import"),
        require("postcss-mixins"),
        require("postcss-url"),
        require("postcss-nested"),
        require("postcss-preset-env"),
        require("postcss-simple-vars"),
        require("autoprefixer"),
    ]
};
