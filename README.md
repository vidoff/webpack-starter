# Webpack Frontend Starterkit

[![Greenkeeper badge](https://badges.greenkeeper.io/wbkd/webpack-starter.svg)](https://greenkeeper.io/)

Webpack starter kit
Легковесный каркас для старта верстки

### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* POSTCSS Support via [postcss-loader](https://github.com/postcss/postcss-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)

